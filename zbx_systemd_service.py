#!/usr/bin/env python3

# This script discovers if a systemd service is enabled
# It returns a JSON, which can be used for Zabbix LLD
#
# Input expects a single string, but can be a regular expression
# In the case of a regex, the result can contain multiple services.
#
# Todo: Input of multiple services

import os
import fnmatch
import subprocess
import json
import argparse

zbx_data = {'data': [], 'values': {}}


def find_service(pattern, path):
    for service in os.listdir(path):
        if fnmatch.fnmatch(service, pattern):
            # Get short name without .service extension
            name = os.path.splitext(service)[0]
            # LLD data for discovery
            zbx_data['data'].append({'{#SERVICE}': service})
            # Values for discovered services
            zbx_data['values'][name] = {
                    'enabled': is_service_enabled(service)
                }


def is_service_enabled(service):
    result = subprocess.Popen(['/usr/bin/systemctl', 'is-enabled', service],
                              stdout=subprocess.PIPE,
                              stderr=subprocess.DEVNULL)

    status = result.stdout.read()
    if 'enabled' in str(status):
        return True
    else:
        return False


def main():

    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description='Zabbix LLD discovery of systemd services')

    parser.add_argument(
        'service',
        type=str,
        help='name of a service file, can be a regular expression')

    args = parser.parse_args()

    # Check system path for systemd files
    find_service(args.service, '/usr/lib/systemd/system')
    # Check path for overriding systemd files
    find_service(args.service, '/etc/systemd/system')

    print(json.dumps(zbx_data))


if __name__ == "__main__":
    main()
