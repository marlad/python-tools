#!/usr/bin/env python3

""" Check if an interface is up and operational """

import fcntl
import struct
import sys
from socket import socket, AF_INET, SOCK_DGRAM

# set some symbolic constants
SIOCGIFFLAGS = 0x8913
null256 = '\0'*256

IFF_UP = 0x1        # Interface up
IFF_RUNNING = 0x40  # interface RFC2863 OPER_UP

status = []

# get the interface name from the command line
ifname = sys.argv[1]

# create a socket so we have a handle to query
s = socket(AF_INET, SOCK_DGRAM)

# call ioctl() to get the flags for the given interface
result = fcntl.ioctl(s.fileno(), SIOCGIFFLAGS, ifname + null256)

# extract the interface's flags from the return value
flags, = struct.unpack('H', result[16:18])

# check "UP" bit and print a message
up = flags & int(hex(IFF_UP), 0)
status.append(('down', 'up')[up])

# check operational status, do we have a carrier link?
bitmask = int(hex(IFF_RUNNING), 0)
running = (flags & bitmask)
if (running == bitmask):
    status.append('operational')
else:
    status.append('not_operational')

status_str = ' '.join(status)
print(f'{ifname}: {status_str}')

# return a value suitable for shell's "if"
sys.exit(not up)
