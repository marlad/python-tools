#!/usr/bin/env python3

"""
  Search for hosts in LDAP and display its description
  Tested with freeIPA.
"""

import sys
import subprocess
import re
import argparse

ldap_base = "cn=computers,cn=accounts,dc=example,dc=com"
hosts = {}
# Option to skip hosts by defining class types
# (will not be listed unless verbose)
skip_classes = ['cname']


def parse_arguments():

    parser = argparse.ArgumentParser(description='Search hosts in LDAP')

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-i',
                       action='store_true',
                       dest='shortname',
                       help='Show short hostnames')
    group.add_argument('-j',
                       action='store_true',
                       dest='fqdn',
                       help='Shown fqdn hostnames')

    parser.add_argument('-v',
                        '--verbose',
                        action='store_true',
                        help='Verbosity')

    parser.add_argument('hostname',
                        type=str,
                        nargs='?',
                        help='(part of) hostname')

    args = parser.parse_args()
    return(args)


def main():
    # Check for script arguments
    args = parse_arguments()
    # Search a hostname (or part of it)
    if args.hostname:
        ldap_filter = "fqdn=*" + args.hostname + "*"
    # Or search all hostnames
    else:
        ldap_filter = "fqdn=*"

    ldap_opts = "-QLLL"

    p = subprocess.Popen(['ldapsearch',
                          '-Y', 'GSSAPI',
                          '-o', 'ldif-wrap=no',
                          '-b', ldap_base,
                          ldap_opts,
                          ldap_filter,
                          'description', 'userClass'],
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE)

    output, error = p.communicate()

    if p.returncode == 0:
        host = ""
        desc = ""
        # ldapsearch divides every record with an empty line.
        # The split on this produces a "multiline" record
        for record in output.split(b'\n\n'):
            host_found = False
            desc = ""
            # Loop per record
            for line in record.split(b'\n'):
                line = line.decode('utf-8')
                valid_host = r'^dn:\s*fqdn=((?!-)[A-Z\d-]{1,63}(?<!-))\.(.+)'
                re_host = re.compile(valid_host, re.IGNORECASE)
                re_description = re.compile(r'^description:\s*(.*)')
                re_userclass = re.compile(r'^userClass:\s*(.*)')

                # DN part
                if re_host.match(line):
                    host_found = True
                    host = re_host.match(line).group(1)
                    domain = re_host.match(line).group(2)

                    if args.shortname:
                        host = '{}'.format(host)
                    elif args.fqdn:
                        host = '{}.{}'.format(host, domain).split(',')[0]
                    hosts[host] = ""

                # Description part
                elif host_found and re_description.match(line):
                    desc = re_description.match(line).group(1)
                    # Add description to host
                    hosts[host] = desc
                # userClass part
                elif host_found and re_userclass.match(line):
                    userclass = re_userclass.match(line).group(1)
                    if args.verbose:
                        # Show all
                        hosts[host] = desc + ' (Class={})'.format(userclass)
                    elif userclass in skip_classes:
                        # or skip
                        del hosts[host]

    else:
        print('LDAP error: {0}'.format(str(p.returncode)))
        sys.exit(2)

    for h in sorted(hosts):
        if not hosts[h]:
            print('{0}: no description available.'.format(h))
        else:
            print('{0}: {1}'.format(h, hosts[h]))


if __name__ == '__main__':
    main()