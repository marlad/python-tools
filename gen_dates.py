#!/usr/bin/env python3

# gen_dates.py:
# This script generates a set of dates

import argparse
import calendar
from datetime import datetime


#
# Return a list with a full week (of days) containing the
# second tuesday of the month
#
def week_of_second_tuesday_of_month(year, month):
    c = calendar.Calendar(firstweekday=calendar.MONDAY)
    monthcal = c.monthdatescalendar(year, month)

    try:
        w = [week for week in monthcal for day in week if
             day.weekday() == calendar.TUESDAY and day.month == month][1]
    except IndexError:
        w = None

    return w


#
# Return a date containing the thursday after the second tuesday
# of the month
# Uses: week_of_second_tuesday_of_month
#
def thursday_after_second_tuesday_of_month(year, month):
    week = week_of_second_tuesday_of_month(year, month)

    try:
        d = [day for day in week if
             day.weekday() == calendar.THURSDAY and day.month == month][0]
    except IndexError:
        d = None

    return d


#
# Return a date containing the last tuesday of the month
#
def last_tuesday_of_month(year, month):
    c = calendar.Calendar(firstweekday=calendar.MONDAY)
    monthcal = c.monthdatescalendar(year, month)

    try:
        d = [day for week in monthcal for day in week if
             day.weekday() == calendar.TUESDAY and day.month == month][-1]
    except IndexError:
        d = None

    return d


#
# Return a date containing the second last tuesday of the month
#
def second_last_tuesday_of_month(year, month):
    c = calendar.Calendar(firstweekday=calendar.MONDAY)
    monthcal = c.monthdatescalendar(year, month)

    try:
        d = [day for week in monthcal for day in week if
             day.weekday() == calendar.TUESDAY and day.month == month][-2]
    except IndexError:
        d = None

    return d


def main():

    year = None

    parser = argparse.ArgumentParser(description='Print Schedule')
    parser.add_argument('-y', '--year',
                        dest=year,
                        type=int,
                        help='If no year is given, it uses the current year')
    parser.add_argument('-e', '--environment',
                        choices=('test', 'acceptance', 'production'),
                        required=True,
                        help='Print schedule for which environment?')
    args = parser.parse_args()

    if not args.year:
        currentDateTime = datetime.now()
        year = int(currentDateTime.date().strftime("%Y"))
    else:
        year = args.year

    if args.environment == 'test':
        for i in range(11):
            print(thursday_after_second_tuesday_of_month(year, i+1))

    if args.environment == 'acceptance':
        for i in range(11):
            print(second_last_tuesday_of_month(year, i+1))

    if args.environment == 'production':
        for i in range(11):
            print(last_tuesday_of_month(year, i+1))


if __name__ == "__main__":
    main()
