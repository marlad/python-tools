#!/usr/bin/env python3

from datetime import datetime, timedelta

days = int(input('How many days: '))

year = int(input('Which year (ie. 2032): '))
month = int(input('Which month (ie. 6): '))
day = int(input('Which day (ie.: 29): '))

to_date = datetime(year=year, month=month, day=day)
result = to_date+timedelta(days=days)
print('{} days from {} = {}'.format(days,
                                    to_date.strftime("%Y-%m-%d"),
                                    result.strftime("%Y-%m-%d")))
