#!/usr/bin/env python3

# Return an idempotent random number, based on username

import hashlib
import getpass
import argparse


# Split a word in a list of single characters
# and convert every character into an integer (base 16)
# return sum of integers
def number(word):
    return sum([int(char, 16) for char in word])


def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description='Return an idempotent random number, based on username')

    parser.add_argument(
        'range',
        type=int,
        help='A range (integer) from which to return a random number')

    args = parser.parse_args()

    name = getpass.getuser()
    hexhash = hashlib.md5(str(name).encode('utf-8'))
    my_number = number(hexhash.hexdigest())

    print(my_number % args.range)


if __name__ == "__main__":
    main()
