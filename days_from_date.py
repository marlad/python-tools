#!/usr/bin/env python3

from datetime import datetime, timedelta

days = int(input('How many days: '))

year = int(input('From which year (ie. 2032): '))
month = int(input('which month (ie. 6): '))
day = int(input('which day (ie.: 29): '))

from_date = datetime(year=year, month=month, day=day)
result = from_date-timedelta(days=days)
print('{} days from {} = {}'.format(days,
                                    from_date.strftime("%Y-%m-%d"),
                                    result.strftime("%Y-%m-%d")))
